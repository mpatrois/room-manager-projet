<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>
  <div class="container">
    <h1>Welcom, this is the rooms list</h1>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Leave</th>
                <th>Enter</th>
                <th>Name</th>
                <th>Occupancy rates</th>
            </tr>
        </thead>
        <tbody>
        <#list rooms as room>
            <#if room.people gt room.capacity*0.70>
                <tr style="background-color:red;">
                <#elseif room.people gt room.capacity*0.40>
                    <tr style="background-color:orange;">
                    <#else>
                        <tr style="background-color:green;">

            </#if>
                <td><img src="qrcode?name=${room.name}&type=-1" style="min-height:100px;height:100px; min-width:100px;width:100px"/>  </td>
                <td><img src="qrcode?name=${room.name}&type=1" style="min-height:100px;height:100px; min-width:100px;width:100px"/>  </td>
                <td>${room.name}</td>
                <td>${room.people}/${room.capacity}</td>
                <td>
                    <a type="button" class="btn btn-success" href="getInGetOut?name=${room.name}&type=1">
                      <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                </td>
                <td>
                    <a type="button" class="btn btn-danger" href="getInGetOut?name=${room.name}&type=-1">
                       <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                    </a>
                </td>
            <tr>
        </#list>
        </tbody>
    </table>
     <a href="admin/roomManagement">login as Admin<a/>
     </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>
  <div class="container">
  <h1>Room management</h1>
<form action="add" name="login" role="form" class="form-horizontal" method="get" accept-charset="utf-8">
                                <div class="form-group">
                                    <div class="col-md-8"><input name="name" placeholder="Name" class="form-control" type="text" ></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-8"><input name="capacity" placeholder="Capacity" class="form-control" type="text" ></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-0 col-md-8"><input class="btn btn-success btn btn-success" type="submit" value="Add room"></div>
                                </div>
</form>
<table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Capacity</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                <#list rooms as room>
                <#if room.people gt room.capacity*0.70>
                    <tr style="background-color:red;">
                    <#elseif room.people gt room.capacity*0.40>
                        <tr style="background-color:orange;">
                        <#else>
                            <tr style="background-color:green;">

                </#if>
                    <td>${room.name}</td>
                    <td>${room.capacity}</td>
                    <td>
                            <a class="btn btn-danger btn btn-success" href="delete?name=${room.name}"  onclick="return confirm('Are you sure to delete')" >Delete</a>
                    </td>
                <tr>
                </#list>
            </tbody>
        </table>
     <a href="../rooms">Rooms list<a/>
 </div>
</body>
</html>
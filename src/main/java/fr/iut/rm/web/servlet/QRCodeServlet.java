package fr.iut.rm.web.servlet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import fr.iut.rm.persistence.dao.RoomDao;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Created by pieagbo on 24/03/15.
 */
@Singleton
public class QRCodeServlet extends HttpServlet {

    /**
     * the dao to access rooms stored in DB *
     */
    @Inject
    RoomDao roomDao;

    /**
     * HTTP GET access
     * @param request use an optional nb parameter to make evidence of transactionnal behavior volontary triggering an exception
     * @param resp response to sent
     * @throws javax.servlet.ServletException by container
     * @throws java.io.IOException by container
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse resp) throws ServletException, IOException {
        QRCodeWriter writer = new QRCodeWriter();
        BitMatrix bitMatrix = null;

        String param = request.getParameter("name");
        String param2= request.getParameter("type");
        int type;
        String url = null;
        try {

            if(param2 !=null && param!=null)
            {
                type = Integer.parseInt(param2);
                url="http://localhost:8080/room-manager/entreeSortie?name="+param+"&type="+type;
            }

            bitMatrix = writer.encode(url, BarcodeFormat.QR_CODE, 300, 300);
        } catch (WriterException e) {
            e.printStackTrace();
        }
        resp.setContentType("image/png");
        MatrixToImageWriter.writeToStream(bitMatrix, "png", resp.getOutputStream());
    }

}

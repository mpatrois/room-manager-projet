package fr.iut.rm.web.servlet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import fr.iut.rm.persistence.dao.RoomDao;
import fr.iut.rm.persistence.domain.Room;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by epatrois on 25/03/15.
 */
@Singleton
public class GetIntGetOut extends HttpServlet {
    /**
     * the dao used to access room persisted data
     */
    @Inject
    RoomDao roomDao;

    /**
     * HTTP GET access
     * @param req use an optional nb parameter to make evidence of transactionnal behavior volontary triggering an exception
     * @param resp response to sent
     * @throws javax.servlet.ServletException by container
     * @throws java.io.IOException by container
     */
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        int type = 0;
        // String name = "";
        String name = req.getParameter("name");
        String typeAction = req.getParameter("type");

        if (typeAction != null) {
            type = Integer.parseInt(typeAction);
        }

        Room r = roomDao.findByName(name);

        if ((r.getPeople() + type <= r.getCapacity()) && (r.getPeople() + type >= 0))
        {
            r.setPeople(r.getPeople() + type);
        }
        roomDao.saveOrUpdate(r);

            resp.sendRedirect("rooms");


    }
}

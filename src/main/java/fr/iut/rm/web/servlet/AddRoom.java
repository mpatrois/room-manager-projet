package fr.iut.rm.web.servlet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import fr.iut.rm.persistence.dao.RoomDao;
import fr.iut.rm.persistence.domain.Room;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by epatrois on 25/03/15.
 */

@Singleton
public class AddRoom extends HttpServlet {

    /**
     * constant for UTF-8 *
     */
    private static final String TEMPLATE_ENCODING = "UTF-8";
    /**
     * the dao to access rooms stored in DB *
     */
    @Inject
    RoomDao roomDao;

    /**
     * HTTP GET access
     * @param req use an optional nb parameter to make evidence of transactionnal behavior volontary triggering an exception
     * @param resp response to sent
     * @throws javax.servlet.ServletException by container
     * @throws java.io.IOException by container
     */
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {

        String name = req.getParameter("name");
        String capacity=req.getParameter("capacity");


        if (name != null && capacity!=null)
        {
                Room room = new Room();
                room.setName(name);
                room.setCapacity(Integer.parseInt(capacity));
                roomDao.saveOrUpdate(room);
        }

       // resp.sendRedirect("gestionSalles");
        resp.sendRedirect("roomManagement");
    }
}